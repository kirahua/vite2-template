import { createApp } from "vue";
import App from "./App";
import { setupStore } from "@store/index";
import setupRouter, { router } from "@router/index";
import { setupDirective } from "./directive";
import { asyncComponent } from "./components";
import { createHead } from "@vueuse/head"; // <--
import { useVisibility } from "./hooks";
import Use_DB from "./hooks/useDB";
import { baseConfig } from "@config/base.config";
import { useSysStore } from "@store/sys";
import { setupGolBalHandler } from "./utils";
import { setupPlugins } from "./plugins";
const app = createApp(App);
setupStore(app); //安装store
setupPlugins(app)
setupDirective(app); //指令
setupRouter(app); //router
asyncComponent(app); //异步组件
setupGolBalHandler(app) //全局数据处理
useVisibility.init(); //趣味功能
app.use(createHead());
/**
 * 设置基础数据库
 */
const db = new Use_DB({
  name: baseConfig.sqlName || "baseSource",
  version: baseConfig.sqlVersion || 1,
  storeName: "base",
});
const sys = useSysStore();
sys.$patch({
  baseSql: db,
});
router
  .isReady()
  .then(() => {
    app.mount("#app");
  })
  .finally(() => {
    if(import.meta.env.MODE.includes("prod")) {
          try {
      app.config.compilerOptions.isCustomElement = tag =>
        tag.startsWith("css-");
    } catch (e) {
      console.log(e);
    }
    }
  });
